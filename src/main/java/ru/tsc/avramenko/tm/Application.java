package ru.tsc.avramenko.tm;

import ru.tsc.avramenko.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (TerminalConst.ABOUT.equals(arg)) showAbout();
        if (TerminalConst.VERSION.equals(arg)) showVersion();
        if (TerminalConst.HELP.equals(arg)) showHelp();
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Avramenko Dmitry");
        System.out.println("E-MAIL: DimaAvramenko@yandex.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConst.ABOUT + " - Display developer information.");
        System.out.println(TerminalConst.VERSION + " - Display program version.");
        System.out.println(TerminalConst.HELP + " - Display list of commands.");
    }

}